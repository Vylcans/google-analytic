<?php

namespace Arcana\AnalyticBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @param string $startDate
     * @return array
     *
     */
    public function indexAction($startDate = null)
    {
        $api = $this->get('arcana.google.api');
        $api->requestAccountData();
        $apiResult = $api->getResults();

        $webPropertyId = $api->getWebPropertyId();

        foreach($apiResult AS $result){
            if($result->getWebPropertyId() == $webPropertyId){
                $profileId = $result->getProfileId();
                break;
            }
        }

        $visitsChartData    = $api->getVisitsGraphData($profileId,$startDate);
        $visitsSummaryData  = $api->getVisitsSummaryData($profileId,$startDate);
        $visitsLanguageData = $api->getVisitsLanguageData($profileId,$startDate);

        if(!$startDate){
            //if no ajax request for data
            $view = 'ArcanaAnalyticBundle:Default:index.html.twig';
        }else{
            //if requested by ajax
            $view = 'ArcanaAnalyticBundle:Default:visitors.html.twig';
        }

        return $this->render($view, array(
            'visitChartData'  => $visitsChartData,
            'summary'         => $visitsSummaryData,
            'languageData'    => $visitsLanguageData,
        ));

    }



}
